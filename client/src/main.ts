import * as THREE from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import {
  ClickableMesh,
  StateMaterialSet,
  MouseEventManager,
  ThreeMouseEventType,
} from '@masatomakino/threejs-interactive-object';
import { Terminal } from 'xterm';
import 'xterm/css/xterm.css';
import './style.css';
import { Drive, FuelTank, Scanner, Ship } from '../../server/components';
import { parse, run } from '../../server/computer';
import { Universe } from '../../server/universe';
import { getNextId } from './id';

let universe = new Universe();
let ship: Ship = { name: 'Nightflyer' };

let clickedRoom: THREE.Group | undefined;

let mousePosition = new THREE.Vector2(0, 0);

const terminal = new Terminal({
  allowTransparency: true,
  theme: {
    background: 'transparent',
  },
  scrollback: 0,
});

interface RoomData {
  label?: HTMLElement;
  appliance?: THREE.Object3D;
  componentName?: string;
}

const applianceFactory: Record<string, () => THREE.Object3D> = {
  tank: () => {
    const tank = tankModel.clone();
    //tank.position.x += 0.22;
    //tank.position.y += 0.22;
    return tank;
  },
  processing: () => {
    const proc = processorModel.clone();
    return proc;
  },
  sensor: () => {
    const sensor = sensorModel.clone();
    return sensor;
  },
  drive: () => {
    const drive = driveModel.clone();
    return drive;
  },
};

const componentFactory: Record<string, () => any> = {
  drive: (): Drive => ({
    min_fuel: 5,
    max_fuel: 5,
    speed_per_fuel_unit: 1,
    current_fuel: 0,
  }),
  tank: (): FuelTank => ({
    capacity: 100,
    content: 100,
  }),
  processing: () => ({
    /* Does not exist in backend */
  }),
  sensor: (): Scanner => ({
    accuracy: 60,
    scope: 30,
    reach: 30,
  }),
};

const buttons = [
  'tank-button',
  'processing-unit-button',
  'sensor-button',
  'drive-button',
];
for (const button of buttons) {
  const b = document.getElementById(button)!;
  b.addEventListener('click', () => {
    const menu = document.getElementById('drop-down-menu')!;
    menu.style.display = 'none';
    if (!clickedRoom) return;

    let data: RoomData = clickedRoom.userData;
    if (data.appliance) {
      clickedRoom.remove(data.appliance);
      delete ship[data.componentName!];
      data.label!.textContent = '';
    }

    const applianceType = button.split('-')[0];
    data.appliance = applianceFactory[applianceType]();
    const componentName = applianceType + '_' + getNextId().toString();
    data.componentName = componentName;
    ship[componentName] = componentFactory[applianceType]();
    data.label!.textContent = componentName;
    clickedRoom.add(data.appliance);
  });
}

const history: string[] = [];
let inputBuffer = '';
let historyPosition = -1;
let cursorPosition = 0;
terminal.attachCustomKeyEventHandler((e) => {
  if (e.type === 'keydown') {
    switch (e.key) {
      case 'ArrowLeft': {
        if (cursorPosition) {
          cursorPosition -= 1;
          terminal.write('\x9BD');
        }
        break;
      }
      case 'ArrowRight': {
        if (cursorPosition < inputBuffer.length) {
          cursorPosition += 1;
          terminal.write('\x9BC');
        }
        break;
      }
      case 'ArrowUp': {
        if (historyPosition === -1) {
          historyPosition = history.length;
        }
        if (historyPosition > 0) {
          historyPosition -= 1;
          inputBuffer = history[historyPosition];
          terminal.write('\x9BM$ ' + inputBuffer);
          cursorPosition = inputBuffer.length;
        }
        break;
      }
      case 'ArrowDown': {
        if (historyPosition === -1) {
          break;
        }
        historyPosition += 1;
        if (historyPosition < history.length) {
          inputBuffer = history[historyPosition];
          terminal.write('\x9BM$ ' + inputBuffer);
          cursorPosition = inputBuffer.length;
        } else {
          historyPosition = -1;
          inputBuffer = '';
          terminal.write('\x9BM$ ');
          cursorPosition = 0;
        }
        break;
      }
      case 'Escape': {
        (document.activeElement as HTMLElement)?.blur();
        break;
      }
      case 'Enter': {
        terminal.write('\r\n');
        handleCommand(inputBuffer);
        history.push(inputBuffer);
        inputBuffer = '';
        terminal.write('$ ');
        cursorPosition = 0;
        historyPosition = -1;
        break;
      }
      case 'Backspace': {
        if (inputBuffer.length) {
          inputBuffer = inputBuffer.slice(0, inputBuffer.length - 1);
          terminal.write('\b\x9BP');
          cursorPosition -= 1;
        }
        break;
      }
      case 'c': {
        if (e.ctrlKey) {
          terminal.write('\r\n');
          inputBuffer = '';
          terminal.write('$ ');
          cursorPosition = 0;
          historyPosition = -1;
        } else {
          addInput(e.key);
        }
        break;
      }
      default: {
        if (e.key.length === 1) {
          addInput(e.key);
        }
        break;
      }
    }
  }
  return true;
});

function addInput(input: string) {
  inputBuffer =
    inputBuffer.slice(0, cursorPosition) +
    input +
    inputBuffer.slice(cursorPosition);
  cursorPosition += 1;
  terminal.write('\x9B@' + input);
}

function handleCommand(command: string) {
  if (command === 'help') {
    terminal.write(
      [
        'pump <amount> from <source> to <destination>',
        'spark <drive>',
        'list devices',
        'show <device>',
        'ship info',
        'run <scanner> <degree> deg',
      ].join('\r\n') + '\r\n',
    );
    return;
  }
  try {
    const output = run(parse(command), ship, universe);
    terminal.write(output.join('\r\n') + '\r\n');
  } catch (ex) {
    terminal.write(`${ex}\r\n`);
  }
}

terminal.open(document.getElementById('terminal')!);
terminal.write('$ ');

const scene = new THREE.Scene();
const aspectRatio = window.innerWidth / window.innerHeight;
/*const camera = new THREE.OrthographicCamera(
  -aspectRatio*2,
  aspectRatio*2,
  2,
  -2,
);*/
const camera = new THREE.PerspectiveCamera(
  40,
  aspectRatio,
  0.1,
  1000,
);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.shadowMap.enabled = true;
document.body.appendChild(renderer.domElement);
const manager = new MouseEventManager(scene, camera, renderer.domElement);
manager;

const modelLoader = new FBXLoader();
const tankModel = await modelLoader.loadAsync(
  'models/Decontamination_section.fbx',
);
tankModel.rotateX(THREE.MathUtils.degToRad(90));
tankModel.castShadow = true;
tankModel.receiveShadow = true;
tankModel.scale.multiplyScalar(0.0007);

const sensorModel = await modelLoader.loadAsync('models/Home_colonists.fbx');
sensorModel.rotateX(THREE.MathUtils.degToRad(90));
sensorModel.castShadow = true;
sensorModel.receiveShadow = true;
sensorModel.scale.multiplyScalar(0.0004);

const processorModel = await modelLoader.loadAsync(
  'models/Research_center.fbx',
);
processorModel.rotateX(THREE.MathUtils.degToRad(90));
processorModel.castShadow = true;
processorModel.receiveShadow = true;
processorModel.scale.multiplyScalar(0.0004);

const driveModel = await modelLoader.loadAsync('models/Solar_generator.fbx');
driveModel.rotateX(THREE.MathUtils.degToRad(90));
driveModel.castShadow = true;
driveModel.receiveShadow = true;
driveModel.scale.multiplyScalar(0.002);

const textureLoader = new THREE.TextureLoader();
const wallTexture = textureLoader.load('textures/TECH_0E.png');
wallTexture.wrapS = THREE.RepeatWrapping;
wallTexture.wrapT = THREE.RepeatWrapping;
wallTexture.repeat.set(4, 1);
const floorTexture = textureLoader.load('textures/FLOOR_1A.png');

const materials = {
  wall: new THREE.MeshPhongMaterial({ map: wallTexture }),
  floor: new THREE.MeshPhongMaterial({ map: floorTexture }),
  wallActive: new THREE.MeshPhongMaterial({
    map: wallTexture,
    color: 0xffaaaa,
  }),
  floorActive: new THREE.MeshPhongMaterial({
    map: floorTexture,
    color: 0xffaaaa,
  }),
};

//const cube = new THREE.Mesh(geometry, material);
const shipGroup = new THREE.Group();

function createWall(horizontal: boolean) {
  const geometry = new THREE.BoxGeometry(1, 0.25, 0.1);
  geometry.rotateX(THREE.MathUtils.degToRad(90));
  geometry.translate(0, 0, 0.1);
  if (!horizontal) {
    geometry.rotateZ(THREE.MathUtils.degToRad(90));
  }
  const mesh = new ClickableMesh({
    geo: geometry,
    material: new StateMaterialSet({
      normal: materials.wall,
      over: materials.wallActive,
    }),
  });
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  return mesh;
}

function createFloor() {
  const geometry = new THREE.BoxGeometry(1, 1, 0.1);
  const mesh = new ClickableMesh({
    geo: geometry,
    material: new StateMaterialSet({
      normal: materials.floor,
      over: materials.floorActive,
    }),
  });
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.position.z -= 0.1;
  return mesh;
}

class Coordinate {
  constructor(public x: number, public y: number) {}

  equals(other: Coordinate) {
    return this.x === other.x && this.y === other.y;
  }

  hashCode() {
    return this.x * 100000 + this.y;
  }

  left() {
    return new Coordinate(this.x - 1, this.y);
  }

  right() {
    return new Coordinate(this.x + 1, this.y);
  }

  top() {
    return new Coordinate(this.x, this.y + 1);
  }

  bottom() {
    return new Coordinate(this.x, this.y - 1);
  }
}

const roomMap = new Map<number, THREE.Group>();

function createRoom(coords: Coordinate) {
  const room = new THREE.Group();

  const label = document.createElement('div');
  label.className = 'room-label';
  room.userData.label = label;
  document.body.appendChild(label);

  const leftWall = createWall(false);
  leftWall.position.x -= 0.5;
  leftWall.addEventListener(ThreeMouseEventType.CLICK, () => {
    const newCoords = coords.left();
    if (!roomMap.has(newCoords.hashCode())) {
      console.log('Creating left room');
      // create new room
      const root = createRoom(newCoords);
      // position relative to current room
      root.position.copy(room.position).add(new THREE.Vector3(-1, 0, 0));
      // add to ship group
      shipGroup.add(root);
      // track in user data
      roomMap.set(newCoords.hashCode(), root);
    }
  });

  const rightWall = createWall(false);
  rightWall.position.x += 0.5;
  rightWall.addEventListener(ThreeMouseEventType.CLICK, () => {
    const newCoords = coords.right();
    if (!roomMap.has(newCoords.hashCode())) {
      console.log('Creating right room');
      // create new room
      const root = createRoom(newCoords);
      // position relative to current room
      root.position.copy(room.position).add(new THREE.Vector3(1, 0, 0));
      // add to ship group
      shipGroup.add(root);
      // track in user data
      roomMap.set(newCoords.hashCode(), root);
    }
  });

  const topWall = createWall(true);
  topWall.position.y += 0.5;
  topWall.addEventListener(ThreeMouseEventType.CLICK, () => {
    const newCoords = coords.top();
    if (!roomMap.has(newCoords.hashCode())) {
      console.log('Creating top room');
      // create new room
      const root = createRoom(newCoords);
      // position relative to current room
      root.position.copy(room.position).add(new THREE.Vector3(0, 1, 0));
      // add to ship group
      shipGroup.add(root);
      // track in user data
      roomMap.set(newCoords.hashCode(), root);
    }
  });

  const bottomWall = createWall(true);
  bottomWall.position.y -= 0.5;
  bottomWall.addEventListener(ThreeMouseEventType.CLICK, () => {
    const newCoords = coords.bottom();
    if (!roomMap.has(newCoords.hashCode())) {
      console.log('Creating bottom room');
      // create new room
      const root = createRoom(newCoords);
      // position relative to current room
      root.position.copy(room.position).add(new THREE.Vector3(0, -1, 0));
      // add to ship group
      shipGroup.add(root);
      // track in user data
      roomMap.set(newCoords.hashCode(), root);
    }
  });

  const floor = createFloor();
  floor.addEventListener(ThreeMouseEventType.CLICK, () => {
    const menu = document.getElementById('drop-down-menu')!;
    menu.style.display = 'block';
    menu.style.left = `${mousePosition.x}px`;
    menu.style.top = `${mousePosition.y}px`;

    clickedRoom = room;
  });

  room.add(leftWall, rightWall, topWall, bottomWall, floor);
  return room;
}

const room1 = createRoom(new Coordinate(0, 0));
shipGroup.add(room1);

const pointLight = new THREE.PointLight(0xff_ff_ff, 1, 100);
const ambientLight = new THREE.AmbientLight(0xff_ff_ff, 1);
pointLight.castShadow = true;
pointLight.position.copy(shipGroup.position).add(new THREE.Vector3(0, 0, 10));

scene.add(shipGroup);
scene.add(pointLight);
scene.add(ambientLight);

camera.position.z = 10;

const inputState = new Map<string, boolean>();

document.addEventListener('keydown', (e) => {
  //e.preventDefault();
  inputState.set(e.key, true);
});

document.addEventListener('keyup', (e) => {
  //e.preventDefault();
  if (e.key === 'Enter') {
    terminal.focus();
  }
  inputState.set(e.key, false);
});

document.addEventListener('pointermove', (e) => {
  mousePosition = new THREE.Vector2(e.clientX, e.clientY);
});

function animate() {
  requestAnimationFrame(animate);

  const velocity = new THREE.Vector3();

  if (inputState.get('ArrowRight')) {
    velocity.x += 1;
  }
  if (inputState.get('ArrowLeft')) {
    velocity.x -= 1;
  }
  if (inputState.get('ArrowUp')) {
    velocity.y += 1;
  }
  if (inputState.get('ArrowDown')) {
    velocity.y -= 1;
  }

  const normVel = velocity.normalize().multiplyScalar(0.05);

  shipGroup.position.add(normVel);

  renderer.render(scene, camera);

  for (const room of shipGroup.children) {
    const roomData: RoomData = room.userData;
    const label = roomData.label!;
    room.updateWorldMatrix(false, false);
    const pos = room
      .getWorldPosition(new THREE.Vector3())
      .project(camera)
      .addScalar(1)
      .multiplyScalar(0.5);
    label.style.left = `${
      pos.x * renderer.domElement.offsetWidth - label.offsetWidth / 2
    }px`;
    label.style.bottom = `${pos.y * renderer.domElement.offsetHeight}px`;
  }
}
animate();
