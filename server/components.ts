export interface FuelTank {
    capacity: number
    content: number
}

export interface Drive {
    speed_per_fuel_unit: number
    min_fuel: number
    max_fuel: number
    current_fuel: number
}

export interface Ship {
    name: string
    [componentName: string]: any
}

export interface Scanner {
    accuracy: number
    reach: number
    scope: number // in deg, cone size of scan
}

export function newShip(): Ship {
    const drive: Drive = {
        min_fuel: 5,
        max_fuel: 5,
        speed_per_fuel_unit: 1,
        current_fuel: 0,
    }

    const tank: FuelTank = {
        capacity: 100,
        content: 100,
    }

    const scanner: Scanner = {
        accuracy: 0.8,
        scope: 30,
        reach: 30,
    }

    return {
        name: "Nightflyer",
        tank_1: tank,
        drive_1: drive,
        scanner_1: scanner,
    }
}
