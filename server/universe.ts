export const SECTOR_SIZE = 100

const HYDROGEN_PROBABILITY = 0.3
const MIN_HYDROGEN = 10
const MAX_HYDROGEN = 100

export interface Asteriod {
    x: number
    y: number
    hydrogen: number
    name: string
    sector: string
}

export type CelestialBody = Asteriod

export interface Sector {
    x: number
    y: number
    celestialBodies: CelestialBody[]
    name: string
}

function generateAsteriod(sectorId: string): Asteriod {
    const hasHydrogen = Math.random() < HYDROGEN_PROBABILITY
    let x = Math.random() * SECTOR_SIZE
    let y = Math.random() * SECTOR_SIZE
    let name = `AST-${x * y}`.replace(".", "")

    return {
        x,
        y,
        name,
        hydrogen: hasHydrogen ? (MAX_HYDROGEN - MIN_HYDROGEN) * Math.random() + MIN_HYDROGEN : 0,
        sector: sectorId,
    }
}

function toDegrees(angle: number): number {
    let degrees = angle * (180 / Math.PI)
    return (360 + degrees) % 360
}

export class Universe {
    private sectors: Record<number, Record<number, Sector>>
    private shipSector: Sector
    private shipX: number
    private shipY: number
    private shipOrientation: number // in deg

    constructor() {
        this.sectors = {}
        this.shipSector = this.generateSector(0, 0)
        this.shipX = SECTOR_SIZE / 2
        this.shipY = SECTOR_SIZE / 2
        this.shipOrientation = 45
    }

    private generateSector(x: number, y: number): Sector {
        const bodyCount = Math.floor(Math.random() * 4) // TODO weighted random: {0: 5%, 1: 15%, 2: 50%, 3: 20%, 4: 10%}
        const name = `SEC-${x < 0 ? "W" : "E"}${y < 0 ? "N" : "S"}'${Math.abs(x)}'${Math.abs(y)}`
        return {
            x,
            y,
            celestialBodies: Array(bodyCount)
                .fill(undefined)
                .map(() => generateAsteriod(name)),
            name,
        }
    }

    // TODO: sector 0, 0 should be the center of the universe
    // player should start in some random sector
    public getSector(x: number, y: number): Sector {
        if (!this.sectors[x]) {
            this.sectors[x] = {}
        }

        if (!this.sectors[x][y]) {
            this.sectors[x][y] = this.generateSector(x, y)
        }

        return this.sectors[x][y]
    }

    public moveShip(speed: number) {
        this.shipX += speed * Math.cos(this.shipOrientation)
        this.shipY += speed * Math.sin(this.shipOrientation)
    }

    public neighboringSectors() {
        let sectors = []
        for (let xOffset of [-1, 0, 1]) {
            for (let yOffset of [-1, 0, 1]) {
                let sect = this.getSector(this.shipSector.x + xOffset, this.shipSector.y + yOffset)
                sectors.push(sect)
            }
        }

        return sectors
    }

    public objectsInCone(orientation: number, degree: number, range: number): Asteriod[] {
        const left = orientation - degree / 2
        const right = orientation + degree / 2

        let result = []
        for (let sect of this.neighboringSectors()) {
            for (let body of sect.celestialBodies) {
                let x = sect.x + body.x
                let y = sect.y + body.y
                let distance = Math.sqrt(Math.pow(x - this.shipX, 2) + Math.pow(x - this.shipY, 2))

                let inRange = distance < range

                let xDirection = x - this.shipX
                let yDirection = y - this.shipY

                let angle = Math.atan(xDirection / yDirection)
                angle = toDegrees(angle)

                let withinCone = angle > left && angle < right

                if (withinCone && inRange) {
                    result.push({ ...body, distance, sector: sect.name })
                }
            }
        }

        return result
    }

    public steerShip(degree: number): number {
        this.shipOrientation = (this.shipOrientation + degree) % 360
        return this.shipOrientation
    }
}
