import { newShip } from "./components"
import { parse, run } from "./computer"
import { Universe } from "./universe"

let universe = new Universe()
let ship = newShip()

//console.log(run(parse("list devices"), ship, universe))
//console.log(run(parse("show tank_1"), ship, universe))
//console.log(run(parse("ship info"), ship, universe))
console.log(run(parse("pump 5 from tank_1 to drive_1"), ship, universe))
console.log(run(parse("spark drive_1"), ship, universe))
console.log(run(parse("run scanner_1 45 deg"), ship, universe))
console.log(run(parse("steer left 50"), ship, universe))
