import { Ship } from "./components"
import { Universe } from "./universe"

export enum InstructionType {
    PumpFuel,
    SparkDrive,
    ListDevices,
    ShowDevice,
    ShipInfo,
    RunScanner,
    Steer,
}

export interface Instruction {
    type: InstructionType
    params: Record<string, string>
}

interface Parser {
    type: InstructionType
    pattern: RegExp
}

function param(name: string): string {
    return `(?<${name}>[^ ]+)`
}

function pattern(...parts: string[]): RegExp {
    return new RegExp(parts.join(" "))
}

const parsers: Parser[] = [
    {
        type: InstructionType.PumpFuel,
        pattern: pattern(
            "pump",
            param("amount"),
            "from",
            param("source"),
            "to",
            param("destination")
        ),
    },
    {
        type: InstructionType.SparkDrive,
        pattern: pattern("spark", param("drive")),
    },
    {
        type: InstructionType.ListDevices,
        pattern: pattern("list devices"),
    },

    {
        type: InstructionType.ShowDevice,
        pattern: pattern("show", param("device")),
    },
    {
        type: InstructionType.ShipInfo,
        pattern: pattern("ship info"),
    },
    {
        type: InstructionType.RunScanner,
        pattern: pattern("run", param("scanner"), param("degree"), "(degree|deg)?"),
    },
    {
        type: InstructionType.Steer,
        pattern: pattern("steer", "(?<direction>left|right)", param("degree")),
    },
]

export function parse(command: string): Instruction {
    for (let parser of parsers) {
        let match = parser.pattern.exec(command)
        if (match) {
            return { type: parser.type, params: match.groups || {} }
        }
    }

    throw new Error("could not parse command")
}

function runPumpFuel(
    { params: { source, destination, amount } }: Instruction,
    ship: Ship
): string[] {
    let fuelAmount = parseInt(amount)
    const fuel_tank = ship[source]
    const drive = ship[destination]

    let new_fuel = drive.current_fuel + fuelAmount

    if (fuel_tank.content < fuelAmount) {
        return [`not enough fuel in ${source}`]
    }

    if (drive.max_fuel < new_fuel) {
        return [`can not pump this much fuel into ${destination}`]
    }

    fuel_tank.content -= fuelAmount
    drive.current_fuel += fuelAmount

    return [`fuel in drive: ${drive.current_fuel}`, `fuel in tank: ${fuel_tank.content}`]
}

function runSparkDrive(
    { params: { drive: drive_name } }: Instruction,
    ship: Ship,
    universe: Universe
): string[] {
    let drive = ship[drive_name]

    if (drive.current_fuel < drive.min_fuel) {
        throw new Error("not enough fuel in drive")
    }
    const speed = drive.current_fuel * drive.speed_per_fuel_unit
    universe.moveShip(speed)
    drive.current_fuel = 0

    return [`accelerated to ${speed}`]
}

function runListDevices(_instr: Instruction, ship: Ship): string[] {
    return Object.keys(ship).filter(k => !SHIP_INFO_FIELDS.includes(k))
}

function runShowDevice(instr: Instruction, ship: Ship): string[] {
    const { device } = instr.params

    return Object.keys(ship[device]).map((k: string) => `${k} ${ship[device][k]}`)
}

function runRunScanner(
    { params: { scanner, degree } }: Instruction,
    ship: Ship,
    universe: Universe
): string[] {
    let spaceObjects = universe.objectsInCone(
        parseInt(degree),
        ship[scanner].scope,
        ship[scanner].reach
    )

    // unique list of sectors
    const sectors = spaceObjects
        .map(({ sector }: any) => sector)
        .filter((sec, index, sectors) => sectors.indexOf(sec) === index)

    const result = []

    if (spaceObjects.length === 0) {
        result.push("no objects found")
    }

    for (let scannedSector of sectors) {
        result.push(`detected objects in sector ${scannedSector}`)
        let objects = spaceObjects.filter(({ sector }) => sector === scannedSector)
        for (let { name, hydrogen, distance } of objects as any) {
            result.push(`found ${name}`)
            result.push(`    distance ${Math.floor(distance * 100) / 100}`)

            // scanner error
            if (Math.random() > ship[scanner].accuracy) {
                if (hydrogen > 0) {
                    hydrogen = 0
                } else {
                    hydrogen = Math.random() * 100 + 10
                }
            }

            if (hydrogen > 0) {
                result.push(`    hydrogen ${Math.floor(hydrogen * 100) / 100}`)
            } else {
                result.push(`    no hydrogen detected`)
            }
        }
    }

    return result
}

function runSteer(
    { params: { direction, degree: degreeStr } }: Instruction,
    _ship: Ship,
    universe: Universe
): string[] {
    let degree = parseInt(degreeStr)
    if (direction === "left") {
        degree = 360 - degree
    }
    let newOrientation = universe.steerShip(degree)
    return [`ship turned to ${newOrientation} degrees`]
}

const SHIP_INFO_FIELDS = ["speed", "weight", "name"]

export function run(instr: Instruction, ship: Ship, universe: Universe): string[] {
    switch (instr.type) {
        case InstructionType.PumpFuel:
            return runPumpFuel(instr, ship)

        case InstructionType.SparkDrive:
            return runSparkDrive(instr, ship, universe)

        case InstructionType.ShipInfo:
            return SHIP_INFO_FIELDS.map(k => `${k}: ${ship[k]}`)

        case InstructionType.ListDevices:
            return runListDevices(instr, ship)

        case InstructionType.ShowDevice:
            return runShowDevice(instr, ship)

        case InstructionType.RunScanner:
            return runRunScanner(instr, ship, universe)

        case InstructionType.Steer:
            return runSteer(instr, ship, universe)

        default:
            return ["could not handle command"]
    }
}
