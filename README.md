# clarkslawiv

## Credits

- Textures: <https://little-martian.itch.io/retro-texture-pack> (CC0 license)
- Models: <https://free-game-assets.itch.io/free-space-colony-3d-low-poly-models>, [viewed on here](https://3dviewer.net/)  ([license](https://craftpix.net/file-licenses/))
